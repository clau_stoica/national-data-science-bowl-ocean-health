import numpy as np
from tensorflow.python.keras.preprocessing import image
from tqdm import tqdm

class ImageToTensorConverter:
    def convertImages(self, imagePaths, targetSize):
        tensors = [self.imageToTensor(imgPath, targetSize) for imgPath in tqdm(imagePaths)]
        return np.vstack(tensors)

    def imageToTensor(self, imgPath, targetSize):
        # loads RGB image as PIL.Image.Image type
        img = image.load_img(imgPath, target_size=targetSize)
        # convert PIL.Image.Image type to 3D tensor
        tensor = image.img_to_array(img)
        # convert 3D tensor to 4D tensor
        return np.expand_dims(tensor, axis=0)

