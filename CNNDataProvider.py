import tensorflow as tf
import numpy as np
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from ImageToTensorConverter import ImageToTensorConverter

class CNNDataProvider:
    def get(self, resizeWidth, resizeHeight):
        data = load_files("data\\train")
        categCount = len(data['target_names'])
        files = np.array(data['filenames'])
        targets = tf.keras.utils.to_categorical(np.array(data['target']), categCount)

        trainAndValidationFiles, testFiles, trainAndValidationTargets, testTargets = train_test_split(files, targets,
                                                                                                      test_size=0.2,
                                                                                                      random_state=42)
        trainFiles, validationFiles, trainTargets, validationTargets = train_test_split(trainAndValidationFiles,
                                                                                        trainAndValidationTargets,
                                                                                        test_size=0.2, random_state=42)

        converter = ImageToTensorConverter()
        trainTensors = converter.convertImages(trainFiles, (resizeWidth, resizeHeight)).astype('float32') / 255
        testTensors = converter.convertImages(testFiles, (resizeWidth, resizeHeight)).astype('float32') / 255
        validTensors = converter.convertImages(validationFiles, (resizeWidth, resizeHeight)).astype('float32') / 255

        return trainTensors, trainTargets, validTensors, validationTargets, testTensors, testTargets, categCount