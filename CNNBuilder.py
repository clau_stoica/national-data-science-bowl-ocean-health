import tensorflow as tf
from tensorflow.python.keras.models import Sequential, Model
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.callbacks import ModelCheckpoint
import numpy as np
from CNNDataProvider import CNNDataProvider
from Metrics import Metrics

class CNNBuilder:
    def build(self, inputWidth, inputHeight, units):
        model = Sequential()
        model.add(Conv2D(filters=64, kernel_size=3, padding='same', activation='relu', input_shape=(inputWidth, inputHeight, 3)))
        model.add(Conv2D(filters=32, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling2D(pool_size=3, strides=2))

        model.add(Conv2D(filters=128, kernel_size=3, padding='same', activation='relu'))
        model.add(Conv2D(filters=64, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling2D(pool_size=3, strides=2))

        model.add(Conv2D(filters=256, kernel_size=3, padding='same', activation='relu'))
        model.add(Conv2D(filters=256, kernel_size=3, padding='same', activation='relu'))
        model.add(Conv2D(filters=128, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling2D(pool_size=3, strides=2))

        model.add(Conv2D(filters=512, kernel_size=3, padding='same', activation='relu'))
        model.add(Conv2D(filters=512, kernel_size=3, padding='same', activation='relu'))
        model.add(Conv2D(filters=256, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling2D(pool_size=3, strides=2))

        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.4))
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.4))
        model.add(Dense(units, activation='softmax'))
        model.summary()

        modelOptimizer = tf.keras.optimizers.Adam(lr=0.0001)
        model.compile(optimizer=modelOptimizer, loss='categorical_crossentropy', metrics=['accuracy'])

        return model

    def train(self, model, trainTensors, trainTargets, validTensors, validationTargets, testTensors, testTargets):
        checkpointer = ModelCheckpoint(filepath='saved_models/weights.best.hdf5', verbose=1, save_best_only=True)
        dataGenerator = ImageDataGenerator(
            width_shift_range=0.1,  # randomly shift images horizontally (10% of total width)
            height_shift_range=0.1,  # randomly shift images vertically (10% of total height)
            rotation_range=90,
            horizontal_flip=True,  # randomly flip images horizontally
            vertical_flip=True,
        )
        dataGenerator.fit(trainTensors)

        batch_size = 20
        model.fit_generator(dataGenerator.flow(trainTensors, trainTargets, batch_size=batch_size),
                            steps_per_epoch=trainTensors.shape[0] // batch_size,
                            epochs=100, callbacks=[checkpointer, Metrics()], verbose=2,
                            validation_data=(validTensors, validationTargets),
                            validation_steps=validTensors.shape[0] // batch_size)

        model.load_weights('saved_models/weights.best.hdf5')
        predictions = [np.argmax(model.predict(np.expand_dims(feature, axis=0))) for feature in testTensors]
        testAccuracy = 100 * np.sum(np.array(predictions) == np.argmax(testTargets, axis=1)) / len(predictions)
        print('Test accuracy: %.4f%%' % testAccuracy)

    def createBenchmark(self, inputWidth, inputHeight):
        dataProvider = CNNDataProvider()
        trainTensors, trainTargets, validTensors, validationTargets, testTensors, testTargets, categCount = dataProvider.get(
            inputWidth, inputHeight)

        base_model = tf.keras.applications.vgg16.VGG16(include_top=False, weights='imagenet', input_shape=(inputWidth, inputHeight, 3))
        x = base_model.output
        x = Flatten()(x)
        predictions = Dense(categCount, activation='softmax')(x)
        model = Model(inputs=base_model.input, outputs=predictions)

        modelOptimizer = tf.keras.optimizers.Adam()
        model.compile(optimizer=modelOptimizer, loss='categorical_crossentropy', metrics=['accuracy'])
        checkpointer = ModelCheckpoint(filepath='saved_models/weights.benchmark.hdf5', verbose=1, save_best_only=True)

        batch_size = 20
        model.fit(x=trainTensors, y=trainTargets, batch_size=batch_size,
                  epochs=15, callbacks=[checkpointer, Metrics()], verbose=2,
                  validation_data=(validTensors, validationTargets))

        model.load_weights('saved_models/weights.benchmark.hdf5')
        predictions = [np.argmax(model.predict(np.expand_dims(feature, axis=0))) for feature in testTensors]
        benchmarkAccuracy = 100 * np.sum(np.array(predictions) == np.argmax(testTargets, axis=1)) / len(predictions)
        print('Benchmark accuracy: %.4f%%' % benchmarkAccuracy)