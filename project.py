from CNNBuilder import CNNBuilder
from CNNDataProvider import CNNDataProvider

dataProvider = CNNDataProvider()
trainTensors, trainTargets, validTensors, validationTargets, testTensors, testTargets, categCount = dataProvider.get(92, 92)

cnnBuilder = CNNBuilder()
model = cnnBuilder.build(92, 92, categCount)
cnnBuilder.train(model, trainTensors, trainTargets, validTensors, validationTargets, testTensors, testTargets)
