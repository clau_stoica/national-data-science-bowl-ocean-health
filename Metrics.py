import numpy as np
from keras.callbacks import Callback
from sklearn.metrics import f1_score, recall_score, precision_score

class Metrics(Callback):
    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.validation_data[0]))).round()
        val_targ = self.validation_data[1]

        val_precision = precision_score(val_targ, val_predict, average="micro")
        print(" — val_precision: %f  (tp / (tp + fp)) " % val_precision)

        val_recall = recall_score(val_targ, val_predict, average="micro")
        print(" — val_recall: %f  (tp / (tp + fn)) " % val_recall)

        return