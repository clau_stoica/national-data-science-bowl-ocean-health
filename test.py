import tensorflow as tf
import numpy as np
from sklearn.datasets import load_files
from ImageToTensorConverter import ImageToTensorConverter
from CNNBuilder import CNNBuilder

data = load_files("data\\test")
files = np.array(data['filenames'])
categCount = len(data['target_names'])

converter = ImageToTensorConverter()
tensors = converter.convertImages(files, (92, 92)).astype('float32') / 255

cnnBuilder = CNNBuilder()
model = cnnBuilder.build(92, 92, 50)
model.load_weights('saved_models/weights.best.hdf5')

targets = tf.keras.utils.to_categorical(np.array(data['target']), categCount)
targets = np.argmax(targets, axis=1)

for index in range(len(tensors)):
    print('file: {} - prediction: {} - target: {}'.format(files[index], np.argmax(model.predict(np.expand_dims(tensors[index], axis=0))), targets[index]))
