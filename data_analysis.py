import os.path
import matplotlib.pyplot as plt

files_count = [len(os.listdir('data/train/' + d)) for d in os.listdir('data/train')]

print("Minimum files count in a category: %d" % min(files_count))
print("Maximum files count in a category: %d" % max(files_count))

plt.plot(files_count)
plt.title('files count by category')
plt.xlabel('Categories')
plt.ylabel('files count')
plt.show()